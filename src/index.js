import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/app';

const root = document.querySelector('#root');
ReactDOM.render(<App />, root);
