import React, { Component } from 'react';
import './app.css';
import Person from "../person/person";

class App extends Component {

    state = {
        persons: [
            { id: "aa", name: "Garry", occupation: "plumber" },
            { id: "bb", name: "David", occupation: "builder" },
            { id: "cc", name: "Joshua", occupation: "goldminer" }
        ],
        showPersons: false
    }

    changeIputValueHandler = (event) => {
        this.setState({
            persons: [
                { name: "Jimmy", occupation: "pharmacist" },
                { name: event.target.value, occupation: "builder" },
                { name: "Joshua", occupation: "goldminer" }
            ]
        })
    };

    togglePersonsHandler = () => {
        this.setState({
            showPersons: !this.state.showPersons
        });
    }

    deletePesonHandler = (index) => {
        let personEach = [...this.state.persons];
        personEach.splice(index, 1);
        this.setState({ persons: personEach });
    }

    render() {

        let personsList = null;

        if (this.state.showPersons) {
            personsList = (
                <div>
                    {this.state.persons.map((person, index) => {
                        return <Person
                            name={person.name}
                            occupation={person.occupation}
                            key={person.id}
                            input={this.changeIputValueHandler}
                            delete={() => this.deletePesonHandler(index)} />;
                    })}
                </div>
            );
        }

        return (
            <div className="app">
                <header className="app-header">
                    <h1>Welcome to React</h1>
                </header>
                <button onClick={this.togglePersonsHandler}>Toggle persons</button>
                {personsList}
            </div>
        );
    }
}

export default App;
