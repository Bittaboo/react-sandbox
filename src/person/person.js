import React from "react";
import './person.css';

const person = (props) => {
    return (
        <div className="person person-card">
            <p className="person person-data" onClick={props.delete}>
                My name is {props.name} and I am a {props.occupation}.
                </p>
                <span className="display-block">
                    {/* <input type="text" onChange={props.input} defaultValue={props.name} /> */}
                    <input type="text" onChange={props.input} />
                </span>
            
        </div>
    )
}

export default person;
